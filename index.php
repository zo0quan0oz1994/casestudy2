<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Favicon-->
	<link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
	<!-- Bootstrap icons-->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
	<!-- Core theme CSS (includes Bootstrap)-->
	<link href="css/styles.css" rel="stylesheet" />
	<title>Shop giày thời trang</title>

</head>

<body>
	<div class="wrapper">
		<?php
		session_start();
		include("admincp/config/config.php");
		include("pages/menu.php");
		include("pages/header.php");
		include("pages/main.php");
		include("pages/footer.php");
		?>
	</div>
</body>

</html>