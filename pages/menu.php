<?php

$sql_danhmuc = "SELECT * FROM tbl_danhmuc ORDER BY id_danhmuc DESC";
$query_danhmuc = mysqli_query($mysqli, $sql_danhmuc);
?>
<?php
if (isset($_GET['dangxuat']) && $_GET['dangxuat'] == 1) {
	unset($_SESSION['dangky']);
}
?>
<!-- Favicon-->
<link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
<!-- Bootstrap icons-->
<link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
<!-- Core theme CSS (includes Bootstrap)-->
<link href="css/styles.css" rel="stylesheet" />
</head>

<body>
	<!-- Navigation-->
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container px-4 px-lg-5">
			<a class="navbar-brand" href="index.php"><strong>Trang chủ</strong></a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
					<?php
					while ($row_danhmuc = mysqli_fetch_array($query_danhmuc)) {
					?>
						<li class="nav-item"><a class="nav-link" href="index.php?quanly=danhmucsanpham&id=<?php echo $row_danhmuc['id_danhmuc'] ?>"><?php echo $row_danhmuc['tendanhmuc'] ?></a></li>
					<?php
					}
					?>
					<li class="nav-item"><a class="nav-link" href="index.php?quanly=giohang">Giỏ hàng</a></li>
				</ul>
				<ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
					<?php
					if (isset($_SESSION['dangky'])) {
					?>
						<li class="nav-item"><a class="nav-link" href="index.php?dangxuat=1">Đăng xuất</a></li>
						<li class="nav-item"><a class="nav-link" href="index.php?quanly=thaydoimatkhau">Thay đổi mật khẩu</a></li>
					<?php
					} else {
					?>
						<li class="nav-item"><a class="btn btn-dark btn-lg" href="index.php?quanly=dangnhap">Đăng nhập</a></li>
						<li class="nav-item"><a class="btn btn-secondary btn-lg" href="index.php?quanly=dangky">Đăng ký</a></li>
					<?php
					}
					?>
				</ul>
				<form action="index.php?quanly=timkiem" method="POST">
					<input type="text" placeholder="Tìm kiếm sản phẩm..." name="tukhoa">
					<input class="btn btn-secondary" type="submit" name="timkiem" value="Tìm kiếm">
				</form>
			</div>
		</div>
	</nav>