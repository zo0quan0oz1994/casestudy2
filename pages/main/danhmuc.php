<?php
$sql_pro = "SELECT * FROM tbl_sanpham WHERE tbl_sanpham.id_danhmuc='$_GET[id]' ORDER BY id_sanpham DESC";
$query_pro = mysqli_query($mysqli, $sql_pro);
//get ten danh muc
$sql_cate = "SELECT * FROM tbl_danhmuc WHERE tbl_danhmuc.id_danhmuc='$_GET[id]' LIMIT 1";
$query_cate = mysqli_query($mysqli, $sql_cate);
$row_title = mysqli_fetch_array($query_cate);
?>
<h3 class="my-3 text-center">Danh mục sản phẩm: <?php echo $row_title['tendanhmuc'] ?></h3>
<section class="pt-1">
	<div class="container px-4 px-lg-5 mt-1">
		<div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
			<?php
			while ($row_pro = mysqli_fetch_array($query_pro)) {
			?>

				<div class="col mb-5">
					<div class="card h-100">
						<!-- Product image-->
						<img class="card-img-top" src="admincp/modules/quanlysp/uploads/<?php echo $row_pro['hinhanh'] ?>" alt="..." />
						<!-- Product details-->
						<div class="card-body p-4">
							<div class="text-center">
								<!-- Product name-->
								<h5 class="fw-bolder"><?php echo $row_pro['tensanpham'] ?></h5>

								<!-- Product price-->
								<p class="price_product">Giá : <?php echo number_format($row_pro['giasp'], 0, ',', '.') . 'vnđ' ?></p>
							</div>
						</div>
						<!-- Product actions-->
						<div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
							<div class="text-center"><a class="btn btn-outline-dark mt-auto" href="index.php?quanly=sanpham&id=<?php echo $row_pro['id_sanpham'] ?>">Chi tiết sản phẩm</a></div>
						</div>
					</div>
				</div>

			<?php
			}
			?>
		</div>
	</div>
</section>