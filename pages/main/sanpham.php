<h3 class="my-3 text-center">Chi tiết sản phẩm</h3>
<div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
	<?php
	$sql_chitiet = "SELECT * FROM tbl_sanpham,tbl_danhmuc WHERE tbl_sanpham.id_danhmuc=tbl_danhmuc.id_danhmuc AND tbl_sanpham.id_sanpham='$_GET[id]' LIMIT 1";
	$query_chitiet = mysqli_query($mysqli, $sql_chitiet);

	while ($row_chitiet = mysqli_fetch_array($query_chitiet)) {
	?>
		<div class="col mb-5">
			<div class="card h-100">
				<!-- Product image-->
				<img class="card-img-top" src="admincp/modules/quanlysp/uploads/<?php echo $row_chitiet['hinhanh'] ?>" alt="..." />
				<!-- Product details-->
				<div class="card-body p-4">
					<div class="text-center">
						<h3 style="margin:0">Tên sản phẩm : <?php echo $row_chitiet['tensanpham'] ?></h3>
						<p>Mã sp: <?php echo $row_chitiet['masp'] ?></p>
						<p>Giá sp: <?php echo number_format($row_chitiet['giasp'], 0, ',', '.') . 'vnđ' ?></p>
						<p>Số lượng sp: <?php echo $row_chitiet['soluong'] ?></p>
						<p>Danh mục sp: <?php echo $row_chitiet['tendanhmuc'] ?></p>
					</div>
				</div>
				<!-- Product actions-->
				<div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
					<form method="POST" action="pages/main/themgiohang.php?idsanpham=<?php echo $row_chitiet['id_sanpham'] ?>">
						<div class="text-center">
							<input class="btn btn-outline-dark mt-auto" name="themgiohang" type="submit" value="Thêm giỏ hàng">
						</div>
					</form>
				</div>
			</div>
		</div>
	<?php
	}
	?>
</div>