<?php
if (isset($_GET['trang'])) {
	$page = $_GET['trang'];
} else {
	$page = 1;
}
if ($page == '' || $page == 1) {
	$begin = 0;
} else {
	$begin = ($page * 3) - 3;
}
$sql_pro = "SELECT * FROM tbl_sanpham,tbl_danhmuc WHERE tbl_sanpham.id_danhmuc=tbl_danhmuc.id_danhmuc ORDER BY tbl_sanpham.id_sanpham DESC LIMIT $begin,6";
$query_pro = mysqli_query($mysqli, $sql_pro);

?>
<h3 class="my-3 text-center">Sản phẩm mới nhất</h3>
<section class="pt-1">
	<div class="container px-4 px-lg-5 mt-1">
		<div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
			<?php
			while ($row = mysqli_fetch_array($query_pro)) {
			?>
				<div class="col mb-5">
					<div class="card h-100">
						<!-- Product image-->
						<img class="card-img-top" src="admincp/modules/quanlysp/uploads/<?php echo $row['hinhanh'] ?>" alt="giay" />
						<!-- Product details-->
						<div class="card-body p-4">
							<div class="text-center">
								<!-- Product name-->
								<h5 class="fw-bolder"><?php echo $row['tensanpham'] ?></h5>
								<!-- Product price-->
								<?php echo number_format($row['giasp'], 0, ',', '.') . 'vnđ' ?> <br>
								<?php echo $row['tendanhmuc'] ?>
							</div>
						</div>
						<!-- Product actions-->
						<div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
							<div class="text-center"><a class="btn btn-outline-dark mt-auto" href="index.php?quanly=sanpham&id=<?php echo $row['id_sanpham'] ?>">Chi tiết sản phẩm</a></div>
						</div>
					</div>
				</div>
			<?php
			}
			?>
		</div>
	</div>
</section>
<?php
$sql_trang = mysqli_query($mysqli, "SELECT * FROM tbl_sanpham");
$row_count = mysqli_num_rows($sql_trang);
$trang = ceil($row_count / 3);
?>
<div style="clear: left">
	<p class="text-center">Trang hiện tại : <?php echo $page ?>/<?php echo $trang ?> </p>
	<ul class="list_trang" style="display: flex;justify-content: center;list-style: none;">

		<?php

		for ($i = 1; $i <= $trang; $i++) {
		?>
			<li style="
				margin: 0px 11px;
				padding: 2px 10px;
				border-radius: 3px;
				
				<?php
				if ($i == $page) {
					echo 'background: #6c757d;';
				} else {
					echo 'background: unset;border: 1px solid black;';
				}
				?>
				">
				<a style="
						color: <?php
								if ($i == $page) {
									echo 'white';
								} else {
									echo '#6c757d';
								}
								?>;
					" href="index.php?trang=<?php echo $i ?>"><?php echo $i ?></a>
			</li>
		<?php
		}
		?>

	</ul>
</div>