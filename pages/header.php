<!-- Header-->
<header class="bg-dark py-5">
	<div class="container px-4 px-lg-5 my-5">
		<div class="text-center text-white">
			<h1 class="display-4 fw-bolder">Panda Shop</h1>
			<p class="lead fw-normal text-white-50 mb-0">TƯƠNG LAI THUỘC VỀ NGƯỜI TIN VÀO VẼ ĐẸP TRONG NHỮNG GIẤC MƠ VÀ BƯỚC ĐI CỦA MÌNH</p>
		</div>
	</div>
</header>