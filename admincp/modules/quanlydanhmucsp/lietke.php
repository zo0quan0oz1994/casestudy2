<?php
$sql_lietke_danhmucsp = "SELECT * FROM tbl_danhmuc ORDER BY thutu DESC";
$query_lietke_danhmucsp = mysqli_query($mysqli, $sql_lietke_danhmucsp);
?>

<main>
  <div class="container mt-3">
    <strong>Liệt kê danh mục sản phẩm</strong>
    <table style="width: 90%;" border="1" style="border-collapse: collapse;" class="table table-success table-striped">
      <tr>
        <th>Id</th>
        <th>Tên danh mục</th>
        <th>Quản lý</th>
      </tr>
      <?php
      $i = 0;
      while ($row = mysqli_fetch_array($query_lietke_danhmucsp)) {
        $i++;
      ?>
        <tr>
          <td><?php echo $i ?></td>
          <td><?php echo $row['tendanhmuc'] ?></td>
          <td>
            <a class="btn btn-success" href="?action=quanlydanhmucsanpham&query=sua&iddanhmuc=<?php echo $row['id_danhmuc'] ?>">Sửa</a> <a class="btn btn-danger" href="modules/quanlydanhmucsp/xuly.php?iddanhmuc=<?php echo $row['id_danhmuc'] ?>">Xoá</a>
          </td>
        </tr>
      <?php
      }
      ?>
    </table>
  </div>
</main>